# ExtensionsAdminBundle

## Описание

данный бандл предназначен для:

* **Изменения имени роутинга в переопределенных бандлах, чтобы темплейты и контроллеры работали корректно**
### пример:

 * до применения роуты строятся с именем переоприделенного банадла например **Nep**ProductBundle:
   * Nitra\_ **Nep**ProductBundle_Category_list
   * Nitra\_ **Nep**ProductBundle_Category_edit
   * Nitra\_ **Nep**ProductBundle_Category_update
   * Nitra\_ **Nep**ProductBundle_Category_object
   * Nitra\_ **Nep**ProductBundle_Category_batch
   * Nitra\_ **Nep**ProductBundle_Category_new
   * Nitra\_ **Nep**ProductBundle_Category_create
   * Nitra\_ **Nep**ProductBundle_Category_filters
   * Nitra\_ **Nep**ProductBundle_Category_scopes

 * после применения **ExtensionsAdminBundle** мы получаем роуты такого вида:
   * Nitra_ProductBundle_Category_list
   * Nitra_ProductBundle_Category_list
   * Nitra_ProductBundle_Category_list
   * Nitra_ProductBundle_Category_object
   * Nitra_ProductBundle_Category_batch
   * Nitra_ProductBundle_Category_new
   * Nitra_ProductBundle_Category_create
   * Nitra_ProductBundle_Category_filters
   * Nitra_ProductBundle_Category_scopes

## Подключение

* **composer.json**

```json
{
    ...   
    "require": {
        ...
        nitra/e-commerce-admin-extensionsbundle": "dev-master",
        ...
    }
    ...
}
```

* **app/AppKernel.php**

```php
<?php

    //...
use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    //...
    public function registerBundles()
    {
        //...
        $bundles = array(
            //...
            new Nitra\ExtensionsAdminBundle\NitraExtensionsAdminBundle(),
            //...
        );
        //...
        return $bundles;
    }
    //...
}
```

### использование:

```yml
NitraProductBundle_admin_nitra_product_bundle_Category:
    resource:   "@NitraNepProductBundle/Controller/Category/"
    type:       overwrite_admingenerator
    prefix:     /category
```