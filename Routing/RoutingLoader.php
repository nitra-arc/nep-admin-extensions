<?php

namespace Nitra\ExtensionsAdminBundle\Routing;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Config\FileLocatorInterface;
use Symfony\Component\Config\Loader\FileLoader;
use Symfony\Component\Config\Resource\FileResource;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;

class RoutingLoader extends FileLoader
{
    /** @var \Symfony\Component\DependencyInjection\ContainerInterface */
    protected $container;

    /** @var \AppKernel */
    protected $kernel;

    /**
     * Constructor.
     *
     * @param \Symfony\Component\Config\FileLocatorInterface $locator
     * @param \Symfony\Component\DependencyInjection\Container $container
     */
    public function __construct(FileLocatorInterface $locator, Container $container)
    {
        parent::__construct($locator);
        $this->container    = $container;
        $this->kernel       = $container->get('kernel');
    }

    // Assoc beetween a controller and is route path
    protected $actions = array(
        'list' => array(
            'pattern'       => '/',
            'defaults'      => array(),
            'requirements'  => array(),
        ),
        'edit' => array(
            'pattern'       => '/{pk}/edit',
            'defaults'      => array(),
            'requirements'  => array(),
        ),
        'update' => array(
            'pattern'       => '/{pk}/update',
            'defaults'      => array(),
            'requirements'  => array(
                '_method'       => 'POST'
            ),
            'controller'    => 'edit',
        ),
        'show' => array(
            'pattern'       => '/{pk}/show',
            'defaults'      => array(),
            'requirements'  => array(),
        ),
        'object' => array(
            'pattern'       => '/{pk}/{action}',
            'defaults'      => array(),
            'requirements'  => array(),
            'controller'    => 'actions',
        ),
        'batch' => array(
            'pattern'       => '/batch',
            'defaults'      => array(),
            'requirements'  => array(
                '_method'       => 'POST'
            ),
            'controller'    => 'actions',
        ),
        'new' => array(
            'pattern'       => '/new',
            'defaults'      => array(),
            'requirements'  => array(),
            'methods'       => array(),
        ),
        'create' => array(
            'pattern'       => '/create',
            'defaults'      => array(),
            'requirements'  => array(
                '_method'       => 'POST'
            ),
            'controller'    => 'new',
        ),
        'filters' => array(
            'pattern'       => '/filter',
            'defaults'      => array(),
            'requirements'  => array(),
            'controller'    => 'list',
        ),
        'scopes' => array(
            'pattern'       => '/scope/{group}/{scope}',
            'defaults'      => array(),
            'requirements'  => array(),
            'controller'    => 'list',
        ),
    );

    protected $yaml = array();

    public function load($resource, $type = null)
    {
        $collection             = new RouteCollection();

        $resource               = str_replace('\\', '/', $resource);
        $this->yaml             = $this->parseYaml($this->getGeneratorFilePath($resource));

        $namespace              = $this->yaml['params']['namespace_prefix'];
        $bundleName             = $this->yaml['params']['bundle_name'];
        $fullBundleName         = $namespace . $bundleName;
        $controllerBundleName   = $this->getBundleNameFromResource($resource);

        foreach ($this->actions as $controller => $datas) {
            $action = 'index';

            $loweredNamespace = str_replace(array('/', '\\'), '_', $namespace);
            if ($controllerFolder = $this->getControllerFolder($resource)) {
                $routeName = $loweredNamespace . '_' . $bundleName . '_' . $controllerFolder . '_' . $controller;
            } else {
                $routeName = $loweredNamespace . '_' . $bundleName . '_' . $controller;
            }

            if (in_array($controller, array('edit', 'update', 'object', 'show')) &&
                null !== $pk_requirement = $this->getFromYaml('params.pk_requirement', null)) {
                $datas['requirements'] = array_merge(
                    $datas['requirements'],
                    array('pk' => $pk_requirement)
                );
            }

            if (isset($datas['controller'])) {
                $action     = $controller;
                $controller = $datas['controller'];
            }

            $controllerName = $resource . ucfirst($controller) . 'Controller.php';
            if (is_file($controllerName)) {
                if ($controllerFolder) {
                    $datas['defaults']['_controller'] = $namespace . '\\'
                        . $controllerBundleName . '\\Controller\\'
                        . $controllerFolder . '\\'
                        . ucfirst($controller) . 'Controller::'
                        . $action . 'Action';
                } else {
                    $datas['defaults']['_controller'] = $loweredNamespace
                        . $controllerBundleName . ':'
                        . ucfirst($controller) . ':' . $action;
                }

                $route = new Route($datas['pattern'], $datas['defaults'], $datas['requirements']);
                $collection->add($routeName, $route);
                $collection->addResource(new FileResource($controllerName));
            }
        }

        // Import other routes from a controller directory (@Route annotation)
        if ($controllerFolder) {
            $annotationRouteName = '@' . $fullBundleName . '/Controller/' . $controllerFolder . '/';
            $collection->addCollection($this->import($annotationRouteName, 'annotation'));
        }

        return $collection;
    }

    public function supports($resource, $type = null)
    {
        return 'overwrite_admingenerator' == $type;
    }

    protected function getControllerFolder($resource)
    {
        $matches = array();
        preg_match('#.+/.+Bundle/Controller?/(.*?)/?$#', $resource, $matches);

        return $matches[1];
    }

    protected function getGeneratorFilePath($resource)
    {
        // Find the *-generator.yml
        $finder = Finder::create()
            ->name($this->getControllerFolder($resource).'-generator.yml')
            ->depth(0)
            ->in(realpath($resource.'/../../Resources/config/'))
            ->getIterator();

        foreach ($finder as $file) {
            return $file->getRealPath();
        }
    }

    /**
     * @param $yaml_path string with point for levels
     */
    public function getFromYaml($yamlPath, $default = null)
    {
        $searchIn = $this->yaml;
        $yamlPath = explode('.', $yamlPath);
        foreach ($yamlPath as $key) {
            if (!isset($searchIn[$key])) {
                return $default;
            }
            $searchIn = $searchIn[$key];
        }

        return $searchIn;
    }

    protected function getBundleNameFromResource($resource)
    {
        $matches = array();
        preg_match('#.+/(.+Bundle)/Controller?/(.*?)/?$#', $resource, $matches);

        return $matches[1];
    }

    protected function parseYaml($file)
    {
        $parent = $this->checkParentGenerator($file);
        $data = Yaml::parse($file);
        if ($parent) {
            $data = $this->mergeGenerators(Yaml::parse($parent), $data);
        }
        return $data;
    }

    protected function checkParentGenerator($file)
    {
        $matches = array();
        preg_match('/.*\\' . DIRECTORY_SEPARATOR . '(.*\\' . DIRECTORY_SEPARATOR . '.*Bundle).*/', $file, $matches);
        list (, $bundle) = preg_replace('/\\' . DIRECTORY_SEPARATOR . '/', '', $matches);
        $Bundles = $this->kernel->getBundle($bundle, false);
        $Bundle = end($Bundles);
        $parentBundleName = $Bundle->getParent();
        if ($parentBundleName !== null) {
            $parentBundles = $this->kernel->getBundle($parentBundleName, false);
            $parentBundle = end($parentBundles);
            $parentGeneratorPath = preg_replace('/.*Bundle/', $parentBundle->getPath(), $file);
            return file_exists($parentGeneratorPath)
                ? $parentGeneratorPath
                : null;
        }
    }

    protected function mergeGenerators($parent, $children)
    {
        foreach ($children as $key => $value) {
            $parentLink = &$parent;
            foreach (explode('.', $key) as $i) {
                $j = trim($i, '\'');
                if (!key_exists($j, $parentLink)) {
                    $parentLink[$j] = array();
                }
                $parentLink = &$parentLink[$j];
            }
            $parentLink = $value;
        }

        return $parent;
    }
}